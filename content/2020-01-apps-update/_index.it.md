---
title: Aggiornamento applicazioni KDE di gennaio 2020

publishDate: 2020-01-09 12:00:00

layout: page # don't translate

summary: "Che cosa è successo questo mese alle applicazioni KDE"

type: announcement # don't translate
---

Siamo nel 2020, stiamo vivendo il futuro, vediamo cosa le applicazioni KDE
ci hanno riservato quest'ultimo mese!

## Conversione di KTimeTracker a KDE Frameworks 5

La tanto attesa versione modernizzata di KTimeTracker è stata finalmente
rilasciata.  L'applicazione è un gestore delle attività personali per le
persone molto impegnate, ora disponibile in Linux, FreeBSD e Windows. Il suo
sviluppo era abbandonato dal 2013 e durante il 2019 è stato convertito in
Qt5 e KDE Frameworks.

{{< img class="text-center" src="ktimetracker.png" link="https://cdn.kde.org/screenshots/ktimetracker/ktimetracker.png" caption="KTimeTracker" >}}

La nuova versione è ripulita e leggermente modernizzata, e le nuove
funzionalità più rilevanti sono la nuova finestra di dialogo di modifica del
tempo di attività e l'anteprima in tempo reale nella finestra di dialogo
Esporta, come mostrato nell'immagine sottostante.

{{< img class="text-center" src="ktimetracker-export.png" link="https://cgit.kde.org/ktimetracker.git/plain/doc/export-times-csv.png" caption="La finestra di esportazione in KTimeTracker" >}}

https://download.kde.org/stable/ktimetracker/5.0.1/src/ È disponibile nella
tua distribuzione Linux o come [programma di installazione per
Windows](https://download.kde.org/stable/ktimetracker/5.0.1/win64/) ed
esiste anche una build notturna [per MacOS non
testata](https://binary-factory.kde.org/job/KTimeTracker_Nightly_macos/).

[Il rilascio è stato annunciato nel blog del responsabile Alexander
Potashev](https://aspotashev.blogspot.com/2019/12/ktimetracker-501-released.html)

## KStars 3.3.9

Il programma per l'astronomia [KStars ha ricevuto nuove funzionalità nella
versione
3.3.9](http://knro.blogspot.com/2020/01/kstars-v339-is-released.html).

Le immagini possono essere regolate con precisione in Ombre, Mezzitoni ed
Evidenziazioni, permettendo la visione delle stelle più tenui.

Costellazioni alternative da Western Sky Culture, che è affascinante da
[leggere](https://sternenkarten.com/2019/12/27/kstars-clines/).

{{< img src="Screenshot_20200102_162452.jpg" caption="Costellazioni alternative" alt="Western Sky Culture" >}}

[KStars è disponibile per](https://edu.kde.org/kstars/) Android, Windows,
MacOS, Snap store e dalla tua distribuzione Linux.

## Framework comuni - KNewStuff

Qui nell'aggiornamento delle applicazioni ci concentriamo sulle applicazioni
piuttosto che sulle librerie del codice. Ma nuove funzionalità nelle
librerie comuni significa anche nuove funzionalità in tutte le applicazioni
:)

Questo mese ci offre un'interfaccia utente riprogettata per KNewStuff,
l'infrastruttura che permette di scaricare componenti aggiuntivi per le tue
applicazioni. La finestra di dialogo di navigazione e scaricamento è stata
riprogettata e la sezione dei commenti ora supporta i filtri. Apparirà a
breve in tutte le applicazioni e nei moduli delle impostazioni, a partire
dal modulo Tema globale che si trova in Impostazioni di sistema.

{{< img src="knewstuff-comments.png" caption="Filtri per i commenti" alt="Filtri per i commenti" style="width: 800px">}}

{{< img src="knewstuff.png" caption="Finestra di navigazione ridisegnata" alt="Finestra di navigazione ridisegnata" style="width: 800px">}}

## Correzioni di errori

[L'aggiornamento mensile di correzione errori di KDevelop
5.4.6](https://www.kdevelop.org/news/kdevelop-546-released) ha risolto un
annoso problema per cui le intestazioni GPL e LGPL erano mischiate,
recuperalo dalla tua distribuzione o dalla Appimage per verificare che l'uso
delle licenze sia corretto.

[Latte Dock
0.9.7](https://psifidotos.blogspot.com/2019/12/latte-bug-fix-release-v097.html)
ha sistemato alcune funzionalità con Qt 5.14 e rimosso diversi errori.

Dolphin Plugins 19.12.1 ha sistemato una finestra di dialogo per commit SVN
danneggiata.

È stata migliorata l'indicizzazione dei file in Elisa.  Sono stati risolti
anche alcuni problemi di compilazione in Android e senza Baloo.

È stato dichiarato che il nuovo rilascio di KPat non ha restrizioni di età
[OARS](https://hughsie.github.io/oars/index.html) rilevanti.

Okular ha risolto un errore di blocco durante la chiusura della finestra per
l'anteprima di stampa.

Il rilascio di questo mese dell'editor video Kdenlive ha visto un numero
elevatissimo di correzioni, in primo luogo nell'aggiornamento delle
[schermate](https://kde.org/applications/multimedia/org.kde.kdenlive)
utilizzate nelle meta-informazioni. Ha ricevuto pure decine di miglioramenti
e correzioni nella gestione della linea temporale e dell'anteprima.

Nel client [LSP](https://langserver.org/) di Kate è ora presente il nuovo
supporto JavaScript.

## Divertiti con KDE nel Flathub Store

KDE sfrutta tutti gli store per applicazioni.  Ora siamo in grado di
recapitarti direttamente sempre più programmi. Uno degli store per
applicazioni principali in Linux è [Flathub](https://flathub.org/home), che
utilizza il formato FlatPak.

Potresti già avere Flatpak e Flathub configurati correttamente e pronti
all'uso nel tuo sistema, in Discover o altri programmi per
l'installazione. Per esempio, KDE neon lo contiene già configurato in modo
predefinito da oltre un anno. Se non li possiedi, esiste una [procedura di
configurazione](https://flatpak.org/setup/) rapida per tutte le
distribuzioni principali.

Se sei interessato ai dettagli tecnici, puoi consultare il [repository di
pacchettizzazione Flatpak di
KDE](https://phabricator.kde.org/source/flatpak-kde-applications/) e leggere
la [guida Flatpak di
KDE](https://community.kde.org/Guidelines_and_HOWTOs/Flatpak) (in inglese).

Ma probabilmente sei interessato alle applicazioni, dai quindi un'occhiata
al [Flathub store nella sezione KDE](https://flathub.org/apps/search/kde).

{{< img src="flathub.png" alt="KDE in Flathub" style="width: 800px" >}}

## LabPlot ora in Chocolatey

[Chocolatey](https://chocolatey.org/) è un gestore di pacchetti per
Windows. Se vuoi avere il pieno controllo di quale software è installato
nella tua macchina Windows o nell'intero gruppo di macchine, allora
Chocolatey ti offre un facile controllo, proprio come sei avvezzo in Linux.

[LabPlot](https://chocolatey.org/packages/labplot) è un'applicazione KDE per
grafici interattivi e l'analisi dei dati scientifici, ed è ora disponibile
con Chocolatey. Provala!

[Blog di
LabPlot](https://labplot.kde.org/2019/09/17/chocolatey-package-for-labplot-available/)

{{< img src="labplot.png" alt="LabPlot in Chocolatey" style="width: 800px" >}}

## Aggiornamenti sito web

Il redivivo Team Web di KDE ha aggiornato molti nostri siti un po'
datati. Il sito web di [KPhotoAlbum](https://www.kphotoalbum.org/), la
nostra applicazione per la ricerca e il salvataggio delle foto, è stato
rilanciato di recente e ne è un buon esempio.

{{< img src="kphotoalbum.png" alt="Nuovo sito di KPhotoAlbum" >}}

E se vuoi ostentare un lettore per musica locale semplice e completo ma ti
vergogni dell'aspetto del vecchio sito web, anche a
[JuK](https://juk.kde.org/) è stato aggiornato da poco il sito web.

## Rilasci 19.12.1

Alcuni dei nostri progetti vengono rilasciati in modo indipendente, altri
vengono rilasciati in massa. Il gruppo 19.12.1 dei progetti è stato
rilasciato oggi e sarà presto disponibile negli app store nelle
distribuzioni.  Consulta la [pagina dei rilasci
19.12.1](https://www.kde.org/announcements/releases/19.12.1.php).  Questo
gruppo era chiamato in precedenza KDE Applications ma tale nome è stato
modificato ed è diventato un servizio di rilascio in modo da evitare
confusione con tutte le altre applicazioni create da KDE e in quanto è
formato da decine di prodotti differenti e non deve essere considerato un
prodotto singolo.
