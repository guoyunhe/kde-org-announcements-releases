---
title: KDE's March 2020 Apps Update

publishDate: 2020-03-02 16:00:00

layout: page # don't translate

summary: "What Happened in KDE's Applications This Month"

type: announcement # don't translate
---

Intro Paragraph

# New releases

## Choqok 1.7

https://choqok.kde.org/news/2020/02/04/choqok-1-7-released.html

{{< img class="text-center" src="foo.png" link="https://www.link" caption="Foo" >}}

# Incoming

CuteHMI
RKWard
Kid3

# App Store

Google Play?
Elisa has been submitted to the Windows Store
https://mgallienkde.wordpress.com/2020/02/01/elisa-has-been-submitted-to-the-windows-store/

Digikam ready to be submitted

# Website Updates

Any new websites this month?

# Releases 19.12.3

Some of our projects release on their own timescale and some get released en-masse. The 19.12.3 bundle of projects was released today and should be available through app stores and distros soon. See the [19.12.3 releases page](https://www.kde.org/info/releases-19.12.3.php) for details. This bundle was previously called KDE Applications but has been de-branded to become a release service to avoid confusion with all the other applications by KDE and because there are dozens of different products rather than a single whole.

Some of the fixes included in this release are:

[19.12.2 release notes](https://community.kde.org/Releases/19.12_Release_Notes) &bull; [Package download wiki page](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro) &bull;  [19.12.1 source info page](https://kde.org/info/releases-19.12.1) &bull; [19.12.1 full changelog](https://kde.org/announcements/changelog-releases.php?version=19.12.1)

* x

# Stores

KDE software is available on many platforms and software app stores.

[![Snapcraft](snapcraft.png)](https://snapcraft.io/publisher/kde)
[![Flathub](flathub.png)](https://flathub.org/apps/search/kde)
[![Microsoft Store](microsoft.png)](https://www.microsoft.com/en-gb/search?q=kde)
