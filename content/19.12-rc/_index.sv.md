---
title: 19.12 RC-utgåvor

summary: "Över 120 individuella program samt dussintalet
programmeringsbibliotek och funktionsinsticksprogram ges samtidigt ut som
del av KDE:s utgivningstjänst."

publishDate: 2019-11-29 00:01:00 # don't translate

layout: page # don't translate

type: announcement # don't translate
---

29:e november , 2019. Över 120 individuella program samt dussintalet
programmeringsbibliotek och funktionsinsticksprogram ges samtidigt ut som
del av KDE:s utgivningstjänst.

Idag ges alla ut som utgivningskandidater, vilket betyder att funktionen är
komplett men behöver testas för slutliga felrättningar.

Distributioner och applikationsbutikpacketerare bör uppdatera sina
förutgåvekanaler för kontrollera om det finns problem.

+ [19.12
versionsfakta](https://community.kde.org/Releases/19.12_Release_Notes) för
information om arkiv och kända problem.  + [Wikisida om
paketnerladdning](https://community.kde.org/Get_KDE_Software_on_Your_Linux_Distro)
+ [Sida om 19.12
RC-källkodsinformation](https://kde.org/info/applications-19.11.90)

## Presskontakter

För mer information skicka e-post till oss:
[press@kde.org](mailto:press@kde.org).
